# Description

This ideapad extra functional source is from Linux Kernel 4.16 source (Linus Torvald Official Github).

Attention! this is a DKMS module, so it need to install using DKMS.

# Changelogs
20/May/2018
- Added support for Linux kernel 4.15

# Installation
1. Clone or download this repo, if you download it, you must extract first.
2. Add it to DKMS as below
   <code>sudo dkms add ./ideapad-laptop-dkms-master</code>
3. Install it using DKMS as below
   <code>sudo dkms install ideapad-laptop/2.1</code>
4. Reboot or restart your GNU/Linux machine to check extra features is functioned.

# Uninstall
1. Uninstall it using DKMS as below
   <code>sudo dkms remove ideapad-laptop/2.1 --all</code>
4. Reboot or restart your GNU/Linux machine.
